<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Motortrak\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }
    
    public function equilibriumIndexAction()
    {
        $eResult = array();
        $elements = $this->params('aelements');
        
        if (!empty($elements)) {
            $arrayElements = explode(',', $elements);

            if ($this->isNumericArray($arrayElements)) {
                $equilibriumService = $this->getServiceLocator()->get('EquilibriumService');
                $eResult = $equilibriumService->getEquilibriumIndex($arrayElements);
            } else {
                $this->getResponse()->setStatusCode(400);
                $eResult = array("400 Error");
            }
        }
        
        return new ViewModel(array(
                        'arrayElements' => $elements,
                        'equilibriumIndex' => empty($eResult)?'':  implode(',', $eResult)
                    )
                );
    }
    
    /**
     * Checks whether the element of an array is numeric or not
     * @param type $array
     * @return boolean
     */
    private function isNumericArray($array)
    {
        foreach ($array as $val) {
            if (!is_numeric($val)) {
                return false;
            }
        }
        return true;
    }

}
