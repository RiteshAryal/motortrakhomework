<?php

namespace Motortrak\Service;

/**
 * This service returns Equilibrium Index of any supplied array elements.
 * 
 * We can either set the array element using setElement($arrayElement) method and call
 * getEquilibriumIndex() method or we can directly call getEquilibriumIndex($arrayElement) 
 * method which basically returns equilibrium index if present else null.
 *
 * @category   Service
 * @author     Ritesh Aryal <riteshrajaryal@yahoo.com>
 * @since      28/09/2014
 * 
 */

class EquilibriumService
{
    /**
     *
     * @var array
     */
    private $aElement;
    
    /**
     * Gets array element
     * @return array
     */
    public function getElement()
    {
        return $this->aElement;
    }
    
    /**
     * Sets array element
     * @param array $element
     * @return \Motortrak\Service\EquilibriumService
     */
    public function setElement(array $element)
    {
        $this->aElement = $element;
        return $this;
    }
    
    /**
     * Function returns Equilibrium Index (one or more) of any supplied array
     * 
     * @param array $elem
     * @return array|null
     * @throws \Exception
     */
    public function getEquilibriumIndex(array $elem=null)
    {
        $result = $elements =array();
        
        if ($elem == null) {
            $elements = $this->getElement();
            if (empty($elements)) {
                throw new \Exception("Array elements not assigned, please either set array elements or send it through parameter.");
            }
        } else {
            $this->setElement($elem);
            $elements = $elem;
        }
        
        $lSum = $index = 0;
        $rSum = array_sum($elements);
        $size = count($elements);
	
	for($index=0; $index<$size; $index++)
	{
		$rSum -= $elements[$index];
		if($lSum == $rSum)
                    $result[] = $index; /*multiple equilibrium index are dumped here and return at the end*/
		
		$lSum += $elements[$index];
	}
	return empty($result)?null:$result;
    }
}
