<?php

namespace MotortrakTest\Service;

use MotortrakTest\Bootstrap;
use Motortrak\Service\EquilibriumService;
use Zend\ServiceManager\ServiceManager;  
use \Mockery as m; 
use PHPUnit_Framework_TestCase;

class EquilibriumServiceTest extends \PHPUnit_Framework_TestCase
{
    protected $equilibriumService;
    
    private $testParam = array(
        'testSimpleWithOneEquilibriumIndex' => array(2,1,7,3),
        'testSimpleWithMultipleEquilibriumIndex' => array(-7,1,5,2,-4,3,0),
        'testWithNoEquilibriumIndex' => array(12,23,34,56,-8,0),
        'testWithLargeArray' => array(1082132608, 0, 1082132608),
        'testSingle' => array(1),
        'testSmallPyramid' => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -9, -8, -7, 
		-6, -5, -4, -3, -2, -1),
        'testException' => array()
    );

    protected function setUp()
    {
        $this->equilibriumService = new EquilibriumService();
    }
    
    public function testSimpleWithOneEquilibriumIndex()
    {
        $result = $this->equilibriumService->getEquilibriumIndex($this->testParam['testSimpleWithOneEquilibriumIndex']);
        $this->assertEquals('2', $result[0]);
    }
    
    public function testSimpleWithOneEquilibriumIndexUsingSetter()
    {
        $this->equilibriumService->setElement($this->testParam['testSimpleWithOneEquilibriumIndex']);
        $result = $this->equilibriumService->getEquilibriumIndex();
        $this->assertEquals('2', $result[0]);
    }
    
    public function testSetterAndGetter()
    {
        $this->equilibriumService->setElement($this->testParam['testSimpleWithOneEquilibriumIndex']);
        $this->assertEquals($this->equilibriumService->getElement(), $this->testParam['testSimpleWithOneEquilibriumIndex']);
    }
    
    public function testSimpleWithTwoEquilibriumIndex()
    {
        $result = $this->equilibriumService->getEquilibriumIndex($this->testParam['testSimpleWithMultipleEquilibriumIndex']);
        $this->assertEquals('3', $result[0]);
        $this->assertEquals('6', $result[1]);
        $this->assertEquals('2', count($result));
    }
    
    public function testWithNoEquilibriumIndex()
    {
        $result = $this->equilibriumService->getEquilibriumIndex($this->testParam['testWithNoEquilibriumIndex']);
        $this->assertEquals('0', count($result));
    }
    
    public function testWithLargeArray()
    {
        $result = $this->equilibriumService->getEquilibriumIndex($this->testParam['testWithLargeArray']);
        $this->assertEquals('1', count($result));  
    }
    
    public function testSingle()
    {
        $result = $this->equilibriumService->getEquilibriumIndex($this->testParam['testSingle']);
        $this->assertEquals('1', count($result));  
    }
    
    public function testSmallPyramid()
    {
        $result = $this->equilibriumService->getEquilibriumIndex($this->testParam['testSmallPyramid']);
        $this->assertEquals('1', count($result));        
    }
    
    /**
     * @expectedException     Exception
     * @expectedExceptionMessage Array elements not assigned, please either set array elements or send it through parameter.
     */
    public function testException()
    {
        $result = $this->equilibriumService->getEquilibriumIndex($this->testParam['testException']);
        $this->assertEquals('0', count($result));
    }
}